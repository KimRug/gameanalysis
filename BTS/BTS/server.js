'use strict';
//--------------------------------------------------------------------------------//
//DB ����
var sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./BTSDB.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
        console.error(err.message);
        //console.error(dbPath);
    } else {
        console.log('Connected to the database.');
    }
});
/*
let sql = 'SELECT * FROM STATIC_Items';
db.all(sql, [], (err, rows) => {
    if (err) {
        throw err;
    }
    rows.forEach((row) => {
        console.log(row);
    });
});
*/
//--------------------------------------------------------------------------------//

//const dbPath = path.resolve(__dirname, './BTSDB.db');
//const jsdom = require('mocha-jsdom');
//global.document = jsdom({ url: "http://localhost" });
var jsdom = require("jsdom");
var { JSDOM } = jsdom;
//global.document = new JSDOM(html).window.document;
//document = new JSDOM(html).window.document;

var events = require('events');
var EventEmitter = new events.EventEmitter;

var http = require('http');
var fs = require('fs');
var path = require('path');

var express = require('express');
var app = express();
var port = 3000;

var bodyParser = require('body-parser');

app.set('HTML', __dirname + '/HTML');

function send404Message(response) {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.write("404 ERROR...");
    response.end();
}
app.listen(port, function () {
    console.log('Server Start');
});

app.use(express.static('HTML'));
app.use(express.static('stylesheet'));
app.use(express.static('champions'));
app.use(express.static('items'));
app.use(express.static('traits'));

app.get('/', function (req, res) {
    fs.readFile('./HTML/Deck.html', function (error, data) {
        if (error) {
            console.log(error);
        } else {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end(data);
        }
    });
});

app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'));

db.close((err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Close the database connection.');
});



