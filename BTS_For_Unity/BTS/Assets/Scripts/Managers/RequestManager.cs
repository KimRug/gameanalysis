﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DesignPattern.Singleton;
using System.Net;
public class RequestManager : SingletonBehavior<RequestManager>
{
    const string APIKEY = "";
    const string HTTPS = "";
    const string ASIAHTTPS = "";
    const string KRHTTPS = "";
}
