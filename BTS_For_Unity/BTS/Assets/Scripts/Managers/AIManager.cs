﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DesignPattern.Singleton;

public class AIManager : SingletonBehavior<AIManager>
{
    public ChampionForAIData mostChamp;
    public Dictionary<string, ChampionForAIData> champions;
    public Dictionary<int, ItemForAIData> items;
    public Dictionary<string, Dictionary<int, ItemForAIData>> championitems;
    public override void Initialize()
    {
        champions = new Dictionary<string, ChampionForAIData>();
        items = new Dictionary<int, ItemForAIData>();
        championitems = new Dictionary<string, Dictionary<int, ItemForAIData>>();
        var championids = ResourcesManager.Instance.Champions.Select(x => x.id).ToArray();
        var itemids = ResourcesManager.Instance.Items.Select(x => x.id).ToArray();

        for(int i =0;i < championids.Length; i++)
        {
            champions.Add(championids[i], new ChampionForAIData(championids[i]));
            championitems.Add(championids[i], new Dictionary<int, ItemForAIData>());

            for (int j = 0; j < itemids.Length; j++)
            {
                championitems[championids[i]].Add(itemids[j], new ItemForAIData(itemids[j],championids[i]));
            }
        }
        for(int i =0;i < itemids.Length; i++)
        {
            items.Add(itemids[i], new ItemForAIData(itemids[i]));
        }
    }
    public string GetFeedBack(MatchData data)
    {
        //현재 챔피언 승률 및 추천 챔피언 선정
        ChampionInGame major = GetMajorChampion(data);
        ChampionForAIData aimajor = GetMajorChampion(data.champions);
        ItemForAIData[] majorItem = GetMajorItem(major);
        ItemForAIData[] aimajorItem = GetMajorItem(aimajor);

        string feedback = MatchDataManager.Instance.userInfo.summonerName + " 님의 이번 게임에서 주력 챔피언은 <color=\"red\">" + major.Name + "</color> 입니다\n" +
            "이 챔피언의 총 승률은 " + champions[major.id].winningRate + "% 이며 \n이 챔피언을 주력 챔피언으로 키울시, 승률은 <color=\"red\">" + champions[major.id].majorWinningRate + "%</color> 입니다\n";
        var tmp = data.champions.Select(x => champions[x.id]).OrderByDescending(x => x.majorWinningRate).ToList();

        //추천 챔피언이 현재 챔피언과 같지 않을때, 챔피언 추천
        if(!major.Name.Equals(aimajor.Name))
        {
            feedback += "하지만, 현재 보유중인 챔피언중 <color=\"red\">" + aimajor.Name + "</color>의 경우 주력 챔피언으로 선정할 경우 승률이 <color=\"red\">" + aimajor.majorWinningRate + "%</color> 입니다.";
        }
        //현재 챔피언의 착용 아이템별 승률 표시
        var tmpitem = major.items.Select(x => championitems[major.id][x.id]).OrderByDescending(x => x.majorWinningRate).ToList();
        feedback += "\n현재 주력 챔피언의 착용 아이템 및 아이템별 승률은 다음과 같습니다.\n";
        for (int i = 0; i < major.items.Count; i++)
        {
            feedback += major.items[i].name + "(착용시 승률 : " + championitems[major.id][major.items[i].id].majorWinningRate + "%)\n";
        }

        //현재 챔피언의 아이템 추천
        var tmpmajorItems = GetMajorItem(major);//championitems[major.id].OrderByDescending(x => x.Value.winningRate).Select(x => x.Value).ToList();
        feedback += "\n"+major.Name+"에는 다음과 같은 아이템이 잘 어울립니다.\n";
        for (int i = 0; i < 3; i++)
        {
            feedback += tmpmajorItems[i].name + "(착용시 승률 : " + tmpmajorItems[i].winningRate + "%)\n";
        }

        if (major.id != aimajor.id)
        {
            tmpmajorItems = GetMajorItem(aimajor);
            feedback += "\n" + aimajor.Name + "에는 다음과 같은 아이템이 잘 어울립니다.\n";
            for (int i = 0; i < 3; i++)
            {
                feedback += tmpmajorItems[i].name + "(착용시 승률 : " + tmpmajorItems[i].winningRate + "%)\n";
            }
        }

        return feedback;
    }
    public ChampionInGame GetMajorChampion(MatchData data)
    {
        var tmp = data.champions.Where(x => x.level == 3 || x.items.Count == 3).OrderByDescending(x => x.cost).ToArray()[0];
        Debug.Log(tmp.Name + " : " + champions[tmp.id].winningRate);
        return tmp;
    }
    public ChampionForAIData GetMajorChampion(ChampionInGame[] data)
    {
        var tmp = data.Select(x => champions[x.id]).OrderByDescending(x => x.majorWinningRate).ToArray()[0];
        Debug.Log(tmp.Name+" : "+tmp.winningRate);
        return tmp;
    }
    public ItemForAIData[] GetMajorItem(Champion data)
    {
        var tmp = championitems[data.id].Select(x=>x.Value).OrderByDescending(x => x.majorWinningRate).ToArray();
        return tmp;
    }
}
