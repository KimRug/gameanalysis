﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DesignPattern.Singleton;
using System.Linq;
public class ResourcesManager : SingletonBehavior<ResourcesManager>
{
    private Dictionary<string, Sprite> _championImages = new Dictionary<string, Sprite>();
    private Dictionary<int, Sprite> _itemImages = new Dictionary<int, Sprite>();
    private Dictionary<string, Sprite> _traitImages = new Dictionary<string, Sprite>();
    public Dictionary<string, Sprite> championImages => _championImages;
    public Dictionary<int, Sprite> itemImages => _itemImages;
    public Dictionary<string, Sprite> traitImages => _traitImages;

    public Champion[] Champions { get; private set; }
    public Item[] Items { get; private set; }
    public Trait[] Traits { get; private set; }

    public override void Initialize()
    {
        var championIds = SQLiteUtil.Query("SELECT * FROM STATIC_Champion").Select(x=>x["championId"].ToString()).ToArray();
        var itemKeys = SQLiteUtil.Query("SELECT * FROM STATIC_Items").Select(x => x["id"].ToString()).ToArray();
        var traitKeys = SQLiteUtil.Query("SELECT * FROM STATIC_Traits").Select(x => x["key"].ToString()).ToArray();

        var championsData = new List<Champion>();
        var itemsData = new List<Item>();
        var traitsData = new List<Trait>();

        for (int i = 0; i < championIds.Length; i++)
        {
            _championImages.Add(championIds[i], Resources.Load<Sprite>("champions/" + championIds[i]));
            championsData.Add(new Champion(championIds[i]));
        }
        for (int i = 0; i < itemKeys.Length; i++)
        {
            _itemImages.Add(int.Parse(itemKeys[i]), Resources.Load<Sprite>("items/" + itemKeys[i]));
            itemsData.Add(new Item(int.Parse(itemKeys[i])));
        }
        for (int i = 0; i < traitKeys.Length; i++)
        {
            _traitImages.Add(traitKeys[i], Resources.Load<Sprite>("traits/" + traitKeys[i]));
            traitsData.Add(new Trait(traitKeys[i]));
        }

        Champions = championsData.ToArray();
        Items = itemsData.ToArray();
        Traits = traitsData.ToArray();
    }
}
