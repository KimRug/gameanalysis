﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DesignPattern.Singleton;

public class MatchDataManager : SingletonBehavior<MatchDataManager>
{
    public UserInfo userInfo = new UserInfo();
    public string[] MatchIds
    {
        get
        {
            List<string> ids = new List<string>();
            var data = SQLiteUtil.Query("SELECT DISTINCT MatchMetaData.matchID FROM MatchMetaData WHERE MatchMetaData.puuid='" + userInfo.puuid + "'");
            for (int i = 0; i < data.Count; i++)
                ids.Add(data[i]["MatchID"].ToString());
            return ids.ToArray();
        }
    }
    public MatchData[] usersMatchDatas
    {
        get
        {
            List<MatchData> tmp = new List<MatchData>();
            string[] ids = MatchIds;
            for (int i = 0; i < ids.Length; i++)
                tmp.Add(new MatchData(userInfo.puuid, ids[i]));

            return tmp.ToArray();
        }
    }
    public MatchData[] GetAllMatchDatas(string matchid)
    {
        string[] puuids = SQLiteUtil.Query("SELECT DISTINCT MatchMetaData.puuID FROM MatchMetaData " +
            "WHERE MatchMetaData.MatchID='" + matchid + "'").Select(x=>x["puuID"].ToString()).ToArray();
        List<MatchData> data = new List<MatchData>();
        for (int i = 0; i < puuids.Length; i++)
            data.Add(new MatchData(puuids[i], matchid));

        return data.ToArray();
    }
    public System.Action onUserLogined;
}
public class UserInfo
{
    public string summonerName;
    public string summonerid;
    public string puuid;
    public string tier;
    public string rank;
    public int level;
}
