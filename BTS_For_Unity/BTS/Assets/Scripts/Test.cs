﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class Test : MonoBehaviour
{
    public GameObject prefab;
    public Transform parent;
    public List<Test> list = new List<Test>();
    public string[] championNames;
    private void Start()
    {
        SQLiteUtil.Initialize();
        ResourcesManager.Instance.Initialize();

        for (int i = 1; i < 9; i++)
            list.Add(Instantiate(prefab, parent).GetComponent<Test>());

        championNames = ResourcesManager.Instance.championImages.Keys.ToArray();
    }
}
