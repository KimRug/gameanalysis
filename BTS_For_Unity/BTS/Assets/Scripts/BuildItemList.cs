﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildItemList : MonoBehaviour
{
    public GameObject itembtn;
    public Transform itemlist;
    public List<BuildItemAdd> list = new List<BuildItemAdd>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i <= 100; i++)
            list.Add(Instantiate(itembtn, itemlist).GetComponent<BuildItemAdd>());
        for (int i = 0; i < list.Count; i++)
            list[i].SetItem(i+1);

    }
}

