﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateDeck : MonoBehaviour
{
    public GameObject DeckList;
    public Transform AddList;
    public List<CreateDeck> list = new List<CreateDeck>();
    // Start is called before the first frame update
    public void deck_add()
    {
        list.Add(Instantiate(DeckList, AddList).GetComponent<CreateDeck>());
    }
}
