﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class cpList : MonoBehaviour
{
    public GameObject btn_cp_img;
    public Transform cplist;
    public List<cpList> list = new List<cpList>();
    public string[] championNames;
    public cplist_build[] cpimg;

    private void Awake()
    {
        SQLiteUtil.Initialize();
        ResourcesManager.Instance.Initialize();
    }
    private void Start()
    {
        for (int i = 1; i <= 58; i++) {
            list.Add(Instantiate(btn_cp_img, cplist).GetComponent<cpList>());
            championNames = ResourcesManager.Instance.championImages.Keys.ToArray();
        }
    }

}
