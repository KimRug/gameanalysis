﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cplist_build : MonoBehaviour
{
    public Image profile, item;

    public void SetChampion(string key)
    {
        profile.sprite = ResourcesManager.Instance.championImages[key];
    }
    public void SetItem(int key)
    {
        item.sprite = ResourcesManager.Instance.itemImages[key];
    }
}
