﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cpTable : MonoBehaviour
{
    public GameObject cpdata;
    public Transform cptable;
    public List<cpTable> list = new List<cpTable>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i <= 9; i++)
            list.Add(Instantiate(cpdata, cptable).GetComponent<cpTable>());
    }

}
