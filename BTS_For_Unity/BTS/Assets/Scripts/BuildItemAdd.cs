﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildItemAdd : MonoBehaviour
{
    public Image item;
    public void SetItem(int key)
    {
        item.sprite = ResourcesManager.Instance.itemImages[key];
    }

}
