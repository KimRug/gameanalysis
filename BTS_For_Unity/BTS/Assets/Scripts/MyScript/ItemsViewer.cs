﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ItemsViewer : MonoBehaviour
{
    public GameObject[] items;
    public System.Action<Item> onClick;
    private void Start()
    {
        int tmp = 0;
        for(int i = 0;i < items.Length; i++)
        {
            int imageKey;
            if (i == 0)
                continue;
            else if (i < 10)
                imageKey = i;
            else if (i % 10 == 0)
                imageKey = ++tmp;
            else
            {
                imageKey = 10 * tmp + i % 10;
                if (!ResourcesManager.Instance.itemImages.ContainsKey(imageKey))
                    imageKey = 10 * (i % 10) + tmp;
            }
            items[i].GetComponent<Image>().sprite = ResourcesManager.Instance.itemImages[imageKey];
            items[i].GetComponent<Button>().onClick.AddListener(() => onClick?.Invoke(new Item(imageKey)));
        }
    }
}
