﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class RecordItem : MonoBehaviour
{
    public MatchData value;
    public MatchData[] otherUserData;
    public Toggle toggleDetail;
    public Button buttonAIFeedback;
    public RecordDetailItem myDetail;
    public GameObject detailRecordPrefab;
    public Transform otherDetailParent;
    public System.Action<bool> onDetailShow;
    public System.Action<MatchData> onClickAIFeedBack;

    private void Awake()
    {
        toggleDetail.onValueChanged.AddListener(value=>onDetailShow?.Invoke(value));
        buttonAIFeedback.onClick.AddListener(()=>onClickAIFeedBack?.Invoke(value));
    }
    public void Set(MatchData value)
    {
        this.value = value;
        var otherpuuid = SQLiteUtil.Query("SELECT DISTINCT MatchMetaData.puuID FROM MatchMetaData " +
            "WHERE MatchMetaData.MatchID='" + value.matchid + "' AND NOT MatchMetaData.puuID='" + value.puuid + "'")
            .Select(x => x["puuID"].ToString()).ToArray();
        List<MatchData> tmp = new List<MatchData>();
        for(int i =0;i < otherpuuid.Length; i++)
        {
            tmp.Add(new MatchData(otherpuuid[i], value.matchid));
        }
        otherUserData = tmp.OrderBy(x=>x.rank).ToArray();
        myDetail.Set(value);
        for (int i = 0; i < otherUserData.Length; i++)
        {
            RecordDetailItem item = Instantiate(detailRecordPrefab, otherDetailParent).GetComponent<RecordDetailItem>();
            item.Set(otherUserData[i]);
        }
    }
    private void OnDestroy()
    {
        onDetailShow = null;
    }
}
