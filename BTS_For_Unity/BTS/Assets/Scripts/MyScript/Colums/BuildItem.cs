﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildItem : DataMonoItem<Build>
{
    public System.Action<BuildItem> onEdit;
    public System.Action<BuildItem> onDelete;
    public GameObject ChampionPrefab;
    public Text textName;
    public Button buttonEdit;
    public Button buttonDelete;
    public Transform championsParent;
    public void Set(Build value)
    {
        this.value = value;
        textName.text = value.Name;
        for (int i = 0; i < value.champions.Count; i++)
        {
            ChampionInGameItem item = Instantiate(ChampionPrefab, championsParent).GetComponent<ChampionInGameItem>();
            item.Set(value.champions[i]);
        }
    }
    private void Awake()
    {
        buttonEdit.onClick.AddListener(() => onEdit?.Invoke(this));
        buttonDelete.onClick.AddListener(() => onDelete?.Invoke(this));
    }

}
