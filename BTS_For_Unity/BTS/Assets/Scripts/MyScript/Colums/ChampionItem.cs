﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChampionItem : DataMonoItem<Champion>
{
    [HideInInspector] 
    public int level;
    [HideInInspector]
    public Item[] items;
    public Image profileBorder;
    public GameObject[] stars;
    public Image profile => profileBorder.transform.GetChild(0).GetComponent<Image>();
    public Image[] itemImages;

    public void Set(Champion value, int level, Item[] items)
    {
        this.value = value;
        this.items = items;
        profile.sprite = value.image;
        Color color = new Color();
        switch (value.cost)
        {
            case 1:
                color = Color.gray;
                break;
            case 2:
                color = Color.green;
                break;
            case 3:
                color = Color.blue;
                break;
            case 4:
                color = Color.magenta;
                break;
            case 5:
                color = Color.yellow;
                break;
        }
        profileBorder.color = color;

        if (items == null)
        {
            for (int i = 0; i < itemImages.Length; i++)
                itemImages[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < itemImages.Length; i++)
        {
            if (i < items.Length)
            {
                itemImages[i].gameObject.SetActive(true);
                itemImages[i].sprite = items[i].image;
            }
            else
                itemImages[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < stars.Length; i++)
        {
            if (i < level)
            {
                stars[i].gameObject.SetActive(true);
                stars[i].GetComponent<Text>().color = color;
            }
            else
                stars[i].gameObject.SetActive(false);
        }
    }
}
