﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraitFilterItem : MonoBehaviour
{
    public Trait trait;
    public Image image;
    public Toggle toggle;
    public System.Action onValueChanged = null;
    // Start is called before the first frame update
    public void Set(Trait trait)
    {
        this.trait = trait;
        image.sprite = ResourcesManager.Instance.traitImages[trait.Name];
        toggle.onValueChanged.AddListener(value =>
        {
            image.color = value ? Color.white : Color.black;
            onValueChanged?.Invoke();
        });
    }
}
