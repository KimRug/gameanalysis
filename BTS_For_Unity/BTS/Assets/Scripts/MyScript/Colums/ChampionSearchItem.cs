﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChampionSearchItem : DataMonoItem<Champion>
{
    public Image profileBorder;
    public Image profile => profileBorder.transform.GetChild(0).GetComponent<Image>();
    public Image costBorder;

    public System.Action<Champion> onSelect;
    Color color
    {
        get
        {
            switch(value.cost)
            {
                case 1:
                    return Color.grey;
                case 2:
                    return Color.green;
                case 3:
                    return Color.blue;
                case 4:
                    return Color.magenta;
                case 5:
                    return Color.yellow;
                default:
                    return Color.grey;
            }
        }
    }
    public void Set(Champion value)
    {
        this.value = value;
        profileBorder.color = color;
        costBorder.color = color;
        costBorder.transform.GetChild(0).GetComponent<Text>().text = value.cost + "$";
        profile.sprite = value.image;
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(()=>onSelect?.Invoke(this.value));
    }
}
