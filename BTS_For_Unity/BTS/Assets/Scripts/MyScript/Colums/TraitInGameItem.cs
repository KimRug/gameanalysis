﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TraitInGameItem : DataMonoItem<TraitInGame>
{
    public Sprite[] bgs;
    public Image BG;
    public Image icon;
    public Text count;
    public void Set(TraitInGame value)
    {
        this.value = value;
        icon.sprite = value.image;
        switch (value.styleID)
        {
            case TraitStyleID.NONE:
                BG.sprite = bgs[0];
                break;
            case TraitStyleID.BRONZE:
                BG.sprite = bgs[1];
                break;
            case TraitStyleID.SILVER:
                BG.sprite = bgs[2];
                break;
            case TraitStyleID.GOLD:
                BG.sprite = bgs[3];
                break;
            case TraitStyleID.CHROMATIC:
                BG.sprite = bgs[4];
                break;
        }
        count.text = value.count.ToString();
    }
}
