﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordDetailItem : DataMonoItem<MatchData>
{
    public Text rank;
    public Dictionary<bool, Trait[]> traits = new Dictionary<bool, Trait[]>();
    public Transform championParent;
    public Transform traitParent;
    public GameObject championPrefab;
    public GameObject traitPrefab;
    public void Set(MatchData value)
    {
        switch(value.rank)
        {
            case 1:
                rank.text = "1st";
                break;
            case 2:
                rank.text = "2nd";
                break;
            case 3:
                rank.text = "3rd";
                break;
            default:
                rank.text = value.rank+"th";
                break;
        }
        for(int i=0; i< value.champions.Length; i++)
        {
            ChampionInGameItem item = Instantiate(championPrefab, championParent).GetComponent<ChampionInGameItem>();
            item.Set(value.champions[i]);
        }
        for(int i = 0;i < value.traits.Length; i++)
        {
            TraitInGameItem item = Instantiate(traitPrefab, traitParent).GetComponent<TraitInGameItem>();
            item.Set(value.traits[i]);
        }
    }
}
