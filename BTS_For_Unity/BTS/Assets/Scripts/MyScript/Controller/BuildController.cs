﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class BuildController : MonoBehaviour
{
    public GameObject buildListObj;
    public Button buttonAddBuild;
    public GameObject buildItemPrefab;
    public Transform buildItemContent;
    public BuildEditController editController;
    public List<Build> buildItems = new List<Build>();
    private void Awake()
    {
        buttonAddBuild.onClick.AddListener(() =>
        {
            buildListObj.SetActive(false);
            editController.gameObject.SetActive(true);
        });
    }
    private void OnEnable()
    {
        LoadBuildItems();
    }
    public void LoadBuildItems()
    {
        buildListObj.SetActive(true);
        editController.gameObject.SetActive(false);
        buildItems.Clear();
        string[] buildnames = SQLiteUtil.Query("SELECT DISTINCT Build.name FROM Build WHERE Build.puuid='" + MatchDataManager.Instance.userInfo.puuid + "'").Select(x => x["name"].ToString()).ToArray();
        for (int i = 0; i < buildnames.Length; i++)
        {
            buildItems.Add(new Build(buildnames[i]));
        }
        buildItemContent.DetachChildren();
        for (int i = 0; i < buildItems.Count; i++)
        {
            BuildItem buildItem = Instantiate(buildItemPrefab, buildItemContent).GetComponent<BuildItem>();
            buildItem.Set(buildItems[i]);
            buildItem.onEdit += (value) =>
            {
                buildListObj.SetActive(false);
                editController.gameObject.SetActive(true);
                editController.Set(value);
            };
            buildItem.onDelete += Delete;

        }
    }
    public void Delete(BuildItem value)
    {
        SQLiteUtil.Query("DELETE FROM Build WHERE Build.name='" + value.value.Name + "' AND Build.puuid='" + MatchDataManager.Instance.userInfo.puuid + "'");
        Debug.Log("DELETE FROM Build WHERE Build.name='" + value.value.Name + "' AND Build.puuid='" + MatchDataManager.Instance.userInfo.puuid + "'");
        LoadBuildItems();
    }
}
