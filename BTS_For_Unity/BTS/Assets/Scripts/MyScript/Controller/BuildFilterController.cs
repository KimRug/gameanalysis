﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class BuildFilterController : MonoBehaviour
{
    public BuildEditController editorController;
    public Button buttonCostAll;
    public Button buttonTraitAll;
    [HideInInspector]
    public TraitFilterItem[] traitFilters;
    public InputField nameSearchField;
    public Toggle[] costFilters;
    public Transform championParent;
    public GameObject championSearchItemPrefab;
    public ChampionSearchItem[] champions;
    public System.Action onValueChanged;

    private string filtedName => nameSearchField.text;
    private List<string> filtedTraits => traitFilters.
        Where(x => x.toggle.isOn).Select(x => x.trait.id).ToList();
    private List<int> filtedCosts => costFilters.
        Where(x => x.isOn).Select(x => int.Parse(x.name)).ToList();
    private void Awake()
    {
        buttonCostAll.onClick.AddListener(() =>
        {
            for (int i = 0; i < costFilters.Length; i++)
                costFilters[i].isOn = true;
        });
        buttonTraitAll.onClick.AddListener(() =>
        {
            for (int i = 0; i < traitFilters.Length; i++)
                traitFilters[i].toggle.isOn = true;
        });
        onValueChanged += OnValueChanged;
        List<ChampionSearchItem> tmpChampionItem = new List<ChampionSearchItem>();
        var tmpchampions = ResourcesManager.Instance.Champions.OrderBy(x => x.cost).ToArray();
        for (int i = 0; i < tmpchampions.Length; i++)
        {
            ChampionSearchItem item = Instantiate(championSearchItemPrefab, championParent).GetComponent<ChampionSearchItem>();
            var champion = ResourcesManager.Instance.Champions[i];
            item.Set(champion);
            item.onSelect += (championData) => editorController.AddChampion(new ChampionInGame(champion.id,1,new Item[] { }));
            tmpChampionItem.Add(item);
        }
        champions = tmpChampionItem.ToArray();

        traitFilters = FindObjectsOfType<TraitFilterItem>().ToArray();
        for (int i = 0; i < traitFilters.Length; i++)
        {
            traitFilters[i].Set(ResourcesManager.Instance.Traits[i]);
            traitFilters[i].onValueChanged += onValueChanged;
        }
        for (int i = 0; i < costFilters.Length; i++)
        {
            costFilters[i].onValueChanged.AddListener(value => onValueChanged?.Invoke());
        }
        nameSearchField.onValueChanged.AddListener((value) => onValueChanged?.Invoke());
    }
    private void OnEnable()
    {
        for (int i = 0; i < costFilters.Length; i++)
            costFilters[i].isOn = true;
        nameSearchField.text = "";
        for (int i = 0; i < traitFilters.Length; i++)
            traitFilters[i].toggle.isOn = true;
    }

    public void OnValueChanged()
    {
        for (int i = 0; i < champions.Length; i++)
        {
            var champion = champions[i];

            if (!string.IsNullOrEmpty(filtedName) && !champion.value.Name.Contains(filtedName))
            {
                Debug.Log(champion.value.Name + "에 " + filtedName + "이 포함 안되어 제거.");
                champion.gameObject.SetActive(false);
            }
            else if (!filtedCosts.Contains(champion.value.cost))
            {
                string log = "[";
                for (int j = 0; j < filtedCosts.Count; j++)
                    log += " " + filtedCosts[j];
                Debug.Log("현재 CostFilter : " + log + "]\n챔피언 비용 : " + champion.value.cost);
                champion.gameObject.SetActive(false);
            }
            else if (champion.value.traits.Where(y => filtedTraits.Contains(y.id)).ToArray().Length == 0)
            {
                string log = "traitFilter : [";
                for (int j = 0; j < filtedTraits.Count; j++)
                    log += " " + filtedTraits[j];
                log += "]\n현재 챔피언 [";
                for (int j = 0; j < champion.value.traits.Length; j++)
                    log += " " + champion.value.traits[j].id;
                Debug.Log(log + "]");
                champion.gameObject.SetActive(false);
            }
            else
            {
                champion.gameObject.SetActive(true);
            }
        }
    }
}
