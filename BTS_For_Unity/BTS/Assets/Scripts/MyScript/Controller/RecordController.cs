﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordController : MonoBehaviour
{
    public GameObject RecordPrefab;
    public Transform recordparent;
    public GameObject popupAIFeedBack;
    public Text feedBack; 
    void OnEnable()
    {
        recordparent.DetachChildren();
        var datas = MatchDataManager.Instance.usersMatchDatas;
        for (int i = 0; i < datas.Length; i++)
        {
            RecordItem item = Instantiate(RecordPrefab, recordparent).GetComponent<RecordItem>();
            item.Set(datas[i]);
            item.onClickAIFeedBack += OnFeedBack;
            AddOnDetail(item);
        }
    }
    public void OnFeedBack(MatchData item)
    {
        popupAIFeedBack.SetActive(true);
        feedBack.text = AIManager.Instance.GetFeedBack(item);
    }
    public void AddOnDetail(RecordItem item)
    {
        item.onDetailShow += (value) =>
        {
            if (value)
            {
                for (int i = 0; i < recordparent.childCount; i++)
                    recordparent.GetChild(i).gameObject.SetActive(false);

                item.gameObject.SetActive(true);
            }
            else
            {
                for (int i = 0; i < recordparent.childCount; i++)
                    recordparent.GetChild(i).gameObject.SetActive(true);
            }
        };
    }
}
