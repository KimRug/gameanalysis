﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsController : MonoBehaviour
{
    public GameObject popup;
    public Image popupProfile;
    public Text description;
    public Text itemName;
    public ItemsViewer viewer;
    private void Awake()
    {
        viewer.onClick += (item) =>
        {
            popup.gameObject.SetActive(true);
            popupProfile.sprite = item.image;
            description.text = item.description;
            itemName.text = item.name;
        };
    }
}
