﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class LoginController : MonoBehaviour
{
    public GameObject mainObject;
    public Dropdown dropdownNames;
    public Button buttonLogin;
    public Button buttonExit;
    private void Awake()
    {
        SQLiteUtil.Initialize();
        ResourcesManager.Instance.Initialize();
        AIManager.Instance.Initialize();

        buttonExit.onClick.AddListener(Application.Quit);
        dropdownNames.options = SQLiteUtil.Query("SELECT * FROM SummonerInfo WHERE tier='IRON' AND rank='I'").Select(x => new Dropdown.OptionData(x["summonerName"].ToString())).ToList();
        buttonLogin.onClick.AddListener(() =>
        {
            var userItem = SQLiteUtil.Query("SELECT * FROM SummonerInfo WHERE SummonerInfo.summonerName='" + dropdownNames.options[dropdownNames.value].text + "'");
            MatchDataManager.Instance.userInfo.summonerid = userItem[0]["summonerId"].ToString();
            MatchDataManager.Instance.userInfo.summonerName = userItem[0]["summonerName"].ToString();
            MatchDataManager.Instance.userInfo.puuid = userItem[0]["puuid"].ToString();
            MatchDataManager.Instance.userInfo.tier = userItem[0]["tier"].ToString();
            MatchDataManager.Instance.userInfo.rank = userItem[0]["rank"].ToString();
            MatchDataManager.Instance.userInfo.level = int.Parse(userItem[0]["summonerLevel"].ToString());
            MatchDataManager.Instance.onUserLogined?.Invoke();
            mainObject.SetActive(true);
            gameObject.SetActive(false);
        });
    }
}
