﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
public class BuildEditController : MonoBehaviour
{
    public InputField buildName;
    public Button buttonSave;
    public Button buttonClear;
    public Text totalCost;
    public Text totalCount;
    public GameObject championPrefab;
    public Transform championParent;
    public GameObject traitPrefab;
    public Transform activatedTraitParent;
    public Transform disableTraitParent;
    public BuildController buildController;
    [HideInInspector]
    public List<ChampionInGame> champions => championParent.GetComponentsInChildren<ChampionInGameItem>().Select(x => x.value).ToList();
    public BuildFilterController filter;
    private void Awake()
    {
        ClearChampion();
        buttonSave.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
            buildController.gameObject.SetActive(true);
            if (string.IsNullOrEmpty(buildName.text))
                return;
            for (int i = 0; i < champions.Count; i++)
            {
                var champion = champions[i];
                string query = "INSERT INTO Build(puuid, name, unitID";
                for (int j = 0; j < champion.items.Count; j++)
                    query += ", item" + j;
                query += ") VALUES('" + MatchDataManager.Instance.userInfo.puuid + "', '" + buildName.text + "', '"+champions[i].id+"'";
                for (int j = 0; j < champion.items.Count; j++)
                    query += ", " + champion.items[j].id;
                SQLiteUtil.Query(query + ")");
            }
            Debug.Log("저장 완료!!");
            buildController.LoadBuildItems();
        });
        buttonClear.onClick.AddListener(ClearChampion);
    }
    public void Set(BuildItem build)
    {
        ClearChampion();
        for (int i = 0; i < build.value.champions.Count; i++)
            AddChampion(build.value.champions[i]);
        buildName.text = build.value.Name;
        SetTexts();
        SetTraits();
    }
    public void SetTexts()
    {
        totalCost.text = champions.Sum(x => x.cost)+"$";
        totalCount.text = "("+champions.Count+ "/10)";
    }
    public void SetTraits()
    {
        activatedTraitParent.DetachChildren();
        disableTraitParent.DetachChildren();
        List<Trait> traits = new List<Trait>();
        for(int i =0;i < champions.Count; i++)
        {
            traits = traits.Union(champions[i].traits).ToList();
        }
        var totalTraits = ResourcesManager.Instance.Traits;
        for (int i =0;i < totalTraits.Length; i++)
        {
            TraitInGameItem item;
            int count = traits.Select(x => x.id).Where(x => x == totalTraits[i].id).Count();
            if (count >= totalTraits[i].styles[0].min)
                item = Instantiate(traitPrefab, activatedTraitParent).GetComponent<TraitInGameItem>();
            else
                item = Instantiate(traitPrefab, disableTraitParent).GetComponent<TraitInGameItem>();

            item.Set(new TraitInGame(totalTraits[i].Name, count));
        }
    }
    public void AddChampion(ChampionInGame champion)
    {
        if (champions.Count < 10)
        {
            var championitem = Instantiate(championPrefab, championParent).GetComponent<ChampionInGameItem>();
            championitem.Set(champion);
        }
        SetTexts();
        SetTraits();
    }
    public void ClearChampion()
    {
        championParent.DetachChildren();
        SetTexts();
        SetTraits();
    }
}
