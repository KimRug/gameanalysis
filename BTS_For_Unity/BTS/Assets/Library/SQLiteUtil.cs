﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using UnityEngine;
public class SQLiteUtil {

    private static string name= "BTSDB";
    private static string ConnectionString = "URI=file:" + Application.persistentDataPath + Path.DirectorySeparatorChar + name+".db";
    public static void Initialize()
    {
        byte[] bytes = Resources.Load<TextAsset>("DB/"+name).bytes;
        if (File.Exists(Application.persistentDataPath + Path.DirectorySeparatorChar + name+".db") ==false)
        {
            FileUtil.Create(Application.persistentDataPath + Path.DirectorySeparatorChar + name+".db", bytes);
        }
    }

    public static List<Hashtable> Select(string tableName, params string[] columns)
    {
        List<Hashtable> list = new List<Hashtable>();

        using (DbConnection dbConn = new SqliteConnection(ConnectionString))
        {
            dbConn.Open();
            string query = "SELECT " + string.Join(", ", columns) + " FROM " + tableName;

            DbCommand dbCommand = dbConn.CreateCommand();
            dbCommand.CommandText = query;

            IDataReader reader = dbCommand.ExecuteReader();

            int columnLength = columns.Length;

            while (reader.Read())
            {
                Hashtable item = new Hashtable();

                foreach (string col in columns)
                {
                    item.Add(col, reader[col]);
                }

                list.Add(item);
            }

            reader.Close();
            dbCommand.Dispose();
            dbConn.Dispose();
        }

        return list;
    }

    public static bool HasAndSelect(string tableName, string[] columnNames, string[] values, bool isAnd=true)
    {
        bool result = false;

        if (columnNames.Length != values.Length)
        {
            Debug.Log("컬럼수와 값이 일치하지 않음");
            return result;
        }

        using (DbConnection dbConn = new SqliteConnection(ConnectionString))
        {
            dbConn.Open();

            DbCommand dbCommand = dbConn.CreateCommand();

            string query = "SELECT " + string.Join(", ", columnNames) + " FROM " + tableName + " WHERE ";

            string plus = "";

            int len = columnNames.Length;
            for (int i = 0; i < len; ++i)
            {
                plus += columnNames[i] + "='" + values[i] + "'";

                if (i < len - 1)
                {
                    plus += isAnd ? " AND " : " OR ";
                }
            }

            query += plus;

            dbCommand.CommandText = query;
            IDataReader reader = dbCommand.ExecuteReader();

            result = reader.Read();

            reader.Close();
            dbCommand.Dispose();
            dbConn.Dispose();
        }
      
        return result;
    }

    public static List<Hashtable> Query(string query)
    {
        List<Hashtable> list = new List<Hashtable>();
        try
        {
            using (DbConnection dbConn = new SqliteConnection(ConnectionString))
            {
                dbConn.Open();

                DbCommand dbCommand = dbConn.CreateCommand();

                dbCommand.CommandText = query;
                IDataReader reader = dbCommand.ExecuteReader();

                while (reader.Read())
                {
                    Hashtable item = new Hashtable();

                    int len = reader.FieldCount;
                    for (int i = 0; i < len; ++i)
                    {
                        item.Add(reader.GetName(i), reader[i]);
                    }

                    list.Add(item);
                }

                reader.Close();
                dbCommand.Dispose();
                dbConn.Dispose();
            }

            return list;
        }
        catch
        {
            Debug.LogError("Query Error!\nQuery : " + query);
            return null;
        }
    }

    public static bool QueryInsert(string query)
    {
        bool result = false;

        using (DbConnection dbConn = new SqliteConnection(ConnectionString))
        {
            dbConn.Open();

            DbCommand dbCommand = dbConn.CreateCommand();
            dbCommand.CommandText = query;

            int row = dbCommand.ExecuteNonQuery();

            result = row > 0;
            dbCommand.Dispose();
            dbConn.Dispose();
        }

        return result;
    }

    public static bool QueryUpdate(string query)
    {
        bool result = false;

        using (DbConnection dbConn = new SqliteConnection(ConnectionString))
        {
            dbConn.Open();

            DbCommand dbCommand = dbConn.CreateCommand();

            dbCommand.CommandText = query;
            int row = dbCommand.ExecuteNonQuery();

            result = row > 0;
            dbCommand.Dispose();
            dbConn.Dispose();
        }

        return result;
    }
    public static bool QueryDelete(string query)
    {
        bool result = false;

        using (DbConnection dbConn = new SqliteConnection(ConnectionString))
        {
            dbConn.Open();

            DbCommand dbCommand = dbConn.CreateCommand();

            dbCommand.CommandText = query;
            int row = dbCommand.ExecuteNonQuery();

            result = row > 0;
            dbCommand.Dispose();
            dbConn.Dispose();
        }

        return result;
    }
}
