﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileUtil {

    public static bool Delete(string filePath)
    {
        if (File.Exists(filePath))
        {
            new FileStream(filePath,FileMode.Truncate).Close();
            return true;
        }
        else
        {
            return false;
        }
    }
    

    public static void Create(string filePath, byte[] data)
    {
        File.Create(filePath).Dispose();

        File.WriteAllBytes(filePath, data);
    }

    /**
     * 파일 로드
     * 기본 경로(Application.persistentDataPath)부터 시작
     */
    public static byte[] Load(string filename)
    {
        string path = Application.persistentDataPath + Path.DirectorySeparatorChar + filename;

        if (File.Exists(path))
        {
            return File.ReadAllBytes(path);
        }
        else
        {
            return null;
        }
    }

    /**
     * 파일 저장
     * 기본 경로(Application.persistentDataPath)부터 시작
     */
    public static void Save(string filename, byte[] data)
    {
        string path = Application.persistentDataPath + Path.DirectorySeparatorChar + filename;

        File.WriteAllBytes(path, data);
    }

    public static string GetFileName(string path)
    {
        int lastIndex = path.LastIndexOf(Path.AltDirectorySeparatorChar) + 1;

        if(lastIndex==0)
        {
            lastIndex = path.LastIndexOf(Path.DirectorySeparatorChar) + 1;
        }
        
        return path.Substring(lastIndex, path.Length - lastIndex);
    }
}
