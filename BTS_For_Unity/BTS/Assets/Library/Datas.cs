﻿using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class Champion
{
    public string Name { get; protected set; }
    public string id { get; protected set; }
    public int cost { get; protected set; }
    public Trait[] traits { get; protected set; }

    public UnityEngine.Sprite image=> ResourcesManager.Instance.championImages[id];
        
    public Champion(string id)
    {
        var sqlItem = SQLiteUtil.Query("SELECT * FROM STATIC_Champion WHERE STATIC_Champion.championId=\'" + id + "\' OR STATIC_Champion.name=\'"+id+"\'");
        if (sqlItem == null || sqlItem.Count == 0)
            throw new System.Exception(id + "에 해당하는 챔피언이 없습니다");

        Name = sqlItem[0]["name"].ToString();
        this.id = id;
        cost = int.Parse(sqlItem[0]["cost"].ToString());

        List<Trait> tmpTraits = new List<Trait>();
        for(int i =0; i<3; i++)
        {
            if (!string.IsNullOrEmpty(sqlItem[0]["trait" + i].ToString()))
                tmpTraits.Add(new Trait(sqlItem[0]["trait" + i].ToString()));
        }
        traits = tmpTraits.ToArray();
    }
}

[System.Serializable]
public class ChampionForAIData : Champion
{
    public int total;
    public int win;
    public int major;
    public int majorwin;

    public float winningRate;
    public float majorWinningRate;
    public ChampionForAIData(string id) : base(id)
    {
        var item = SQLiteUtil.Query("SELECT * FROM AI_ChampionData WHERE championid='" + id + "'");
        total = int.Parse(item[0]["total"].ToString());
        win = int.Parse(item[0]["win"].ToString());
        major = int.Parse(item[0]["major"].ToString());
        majorwin = int.Parse(item[0]["majorwin"].ToString());
        if (win == 0 || total == 0 || major == 0 || majorwin == 0)
        {
            winningRate = 0;
            majorWinningRate = 0;
        }
        else
        {
            winningRate = UnityEngine.Mathf.Round((float)win / (float)total * 10000f) / 100f;
            majorWinningRate = UnityEngine.Mathf.Round((float)majorwin / (float)major * 10000f) / 100f;
        }
        string log = Name;
        log += "\ntotal = " + total;
        log += "\nwin = " + win;
        log += "\nmajor = " + major;
        log += "\nmajorwin = " + majorwin;
        log += "\nwinningRate = " + winningRate;
        log += "\nmajorWinningRate = " + majorWinningRate;
    }
}

[System.Serializable]
public class ChampionInGame : Champion
{
    public int level;
    public List<Item> items;
    public ChampionInGame(string id, int level, Item[] items) : base(id)
    {
        this.level = level;
        this.items = items.ToList();
    }
}

[System.Serializable]
public class Trait
{
    public string Name { get; private set; }
    public string id { get; private set; }
    public TraitStyle[] styles { get; private set; }
    public UnityEngine.Sprite image => ResourcesManager.Instance.traitImages[id];
    public Trait(string name)
    {
        name = name.Replace(" ", "");
        var sqlItem = SQLiteUtil.Query("SELECT * FROM STATIC_Traits WHERE STATIC_Traits.key=\'" + name + "\' OR STATIC_Traits.name=\'"+name+"\'");
        if (sqlItem == null || sqlItem.Count == 0)
            throw new System.Exception(name + "에 해당하는 시너지가 없습니다\n"+ "SELECT * FROM STATIC_Traits WHERE STATIC_Traits.key=\'" + name + "\' OR STATIC_Traits.name=\'" + name + "\'");

        this.id = sqlItem[0]["key"].ToString();
        this.Name = name;
        sqlItem = SQLiteUtil.Query("SELECT * FROM STATIC_TraitsStyle WHERE STATIC_TraitsStyle.key=\'" + id + "\'");
        
        List<TraitStyle> tmp = new List<TraitStyle>();
        for (int i = 0; i < sqlItem.Count; i++)
            tmp.Add(new TraitStyle(sqlItem[i]["styleId"].ToString(), 
                int.Parse(sqlItem[i]["min"].ToString()), 
                int.Parse(sqlItem[i]["max"].ToString()), 
                int.Parse(sqlItem[i]["grade"].ToString()) +1));
        styles = tmp.OrderBy(x => x.min).ToArray();
    }
}
[System.Serializable]
public class TraitInGame : Trait
{
    public int count;
    public int grade;
    public bool Activate;
    public TraitStyleID styleID;
    public TraitInGame(string name, int count) : base(name)
    {
        var stlys = styles.OrderBy(x => x.min).ToArray();
        styleID = TraitStyleID.NONE;
        grade = 0;
        this.count = count;
        Activate = false;
        for (int i = 0; i < styles.Length; i++)
        {
            if (styles[i].min <= count)
            {
                styleID = styles[i].styleID;
                grade = styles[i].grade;
                Activate = true;
            }
        }

    }
}

[System.Serializable]
public class TraitStyle
{
    public int min { get; private set; }
    public int max { get; private set; }
    public int grade { get; private set; }
    public TraitStyleID styleID;
    public TraitStyle(string styleID, int min, int max, int grade)
    {
        this.min = min;
        this.max = max;
        this.grade = grade;
        switch (styleID)
        {
            case "bronze":
                this.styleID = TraitStyleID.BRONZE;
                break;
            case "gold":
                this.styleID = TraitStyleID.GOLD;
                break;
            case "silver":
                this.styleID = TraitStyleID.SILVER;
                break;
            case "chromatic":
                this.styleID = TraitStyleID.CHROMATIC;
                break;
        }
    }
}

[System.Serializable]
public class Item
{
    public int id { get; private set; }
    public string name { get; private set; }
    public string description { get; private set; }
    public UnityEngine.Sprite image => ResourcesManager.Instance.itemImages[id];
    public Item(int id)
    {
        var sqlItem = SQLiteUtil.Query("SELECT * FROM STATIC_Items WHERE STATIC_Items.id=\'" + id + "\' OR STATIC_Items.name=\'"+id+"\'");
        if (sqlItem == null || sqlItem.Count == 0)
            throw new System.Exception(name + "에 해당하는 아이템이 없습니다");

        this.id = id;
        name = sqlItem[0]["name"].ToString();
        description = sqlItem[0]["description"].ToString();
    }
}

[System.Serializable]
public class ItemForAIData : Item
{
    public int total;
    public int win;
    public int major;
    public int majorwin;
    public float winningRate;
    public float majorWinningRate;
    public ItemForAIData(int itemid) : base(itemid)
    {
        var championids = ResourcesManager.Instance.Champions.Select(x => x.id).ToArray();
        var item = SQLiteUtil.Query("SELECT * FROM AI_ChampionItems WHERE itemid=" + itemid + "");
        total = item.Sum(x => int.Parse(x["count"].ToString()));
        win = item.Sum(x => int.Parse(x["win"].ToString()));
        major = item.Sum(x => int.Parse(x["major"].ToString()));
        majorwin = item.Sum(x => int.Parse(x["majorwin"].ToString()));
        if (win == 0 || total == 0 || major == 0 || majorwin == 0)
        {
            winningRate = 0;
            majorWinningRate = 0;
        }
        else
        {
            winningRate = UnityEngine.Mathf.Round((float)win / (float)total * 10000f) / 100f;
            majorWinningRate = UnityEngine.Mathf.Round((float)majorwin / (float)major * 10000f) / 100f;
        }
        string log = name;
        log += "\ntotal = " + total;
        log += "\nwin = " + win;
        log += "\nmajor = " + major;
        log += "\nmajorwin = " + majorwin;
        log += "\nwinningRate = " + winningRate;
        log += "\nmajorWinningRate = " + majorWinningRate;
    }
    public ItemForAIData(int itemid, string champid) : base(itemid)
    {
        var championids = ResourcesManager.Instance.Champions.Select(x => x.id).ToArray();
        var item = SQLiteUtil.Query("SELECT * FROM AI_ChampionItems WHERE championid='" + champid + "' AND itemid="+itemid);
        total = item.Sum(x => int.Parse(x["count"].ToString()));
        win = item.Sum(x => int.Parse(x["win"].ToString()));
        major = item.Sum(x => int.Parse(x["major"].ToString()));
        majorwin = item.Sum(x => int.Parse(x["majorwin"].ToString()));
        if (win == 0 || total == 0 || major == 0 || majorwin == 0)
        {
            winningRate = 0;
            majorWinningRate = 0;
        }
        else
        {
            winningRate = UnityEngine.Mathf.Round((float)win / (float)total*10000f)/100f;
            majorWinningRate = UnityEngine.Mathf.Round((float)majorwin / (float)major * 10000f)/100f;
        }
        string log = name+" : "+champid;
        log += "\ntotal = " + total;
        log += "\nwin = " + win;
        log += "\nmajor = " + major;
        log += "\nmajorwin = " + majorwin;
        log += "\nwinningRate = " + winningRate;
        log += "\nmajorWinningRate = " + majorWinningRate;
    }
}

[System.Serializable]
public class MatchData
{
    public string puuid;
    public string matchid;
    public int rank;
    public int gold_left;
    public int level;
    public int round;
    public ChampionInGame[] champions { get; private set; }
    public TraitInGame[] traits { get; private set; }
    public MatchData(string puuid, string matchid)
    {
        this.puuid = puuid;
        this.matchid = matchid;
        var matchData = SQLiteUtil.Query(
            "SELECT MatchMetaData.*, MatchUnits.unitID, MatchUnits.level as unitLevel, MatchUnits.item0, MatchUnits.item1, MatchUnits.item2 FROM MatchMetaData " +
            "LEFT OUTER JOIN MatchUnits ON " +
            "MatchMetaData.MatchID=MatchUnits.matchID AND MatchMetaData.puuID=MatchUnits.puuid " +
            "WHERE MatchMetaData.MatchID='" + matchid + "' AND MatchMetaData.puuID='" + puuid + "'");
        rank = int.Parse(matchData[0]["rank"].ToString());
        gold_left = int.Parse(matchData[0]["gold_left"].ToString());
        level = int.Parse(matchData[0]["level"].ToString());
        round = int.Parse(matchData[0]["lastRound"].ToString());
        List<ChampionInGame> champions = new List<ChampionInGame>();
        for (int i =0;i < matchData.Count; i++)
        {
            List<Item> tmpitems = new List<Item>();
            string id = matchData[i]["unitID"].ToString();
            int level = int.Parse(matchData[i]["unitLevel"].ToString());
            for (int j = 0; j < 3; j++)
            {
                var iteminfo = matchData[i]["item" + j];
                int itemID;
                if (iteminfo != null && int.TryParse(iteminfo.ToString(), out itemID))
                    tmpitems.Add(new Item(itemID));
            }
            champions.Add(new ChampionInGame(id,level,tmpitems.ToArray()));
        }
        this.champions = champions.ToArray();
        Dictionary<string, int> tmp = new Dictionary<string, int>();
        for(int i =0;i < this.champions.Length; i++)
        {
            for(int j = 0; j < this.champions[i].traits.Length; j++)
            {
                if (tmp.ContainsKey(this.champions[i].traits[j].Name))
                    tmp[this.champions[i].traits[j].Name] += 1;
                else
                    tmp.Add(this.champions[i].traits[j].Name,1);
            }
        }
        List<TraitInGame> traitInGames = new List<TraitInGame>();
        foreach (var item in tmp)
        {
            traitInGames.Add(new TraitInGame(item.Key, item.Value));
        }
        this.traits = traitInGames.ToArray();
    }

    public bool ContainsChampion(string championid)=> champions.Select(x=>x.id).Contains(championid);
    public bool ContainsTrait(string traitid) => traits.Select(x => x.id).Contains(traitid);
    public ChampionInGame GetChampion(string championid)
    {
        if (ContainsChampion(championid))
            return champions.Where(x => x.id == championid).ToArray()[0];
        else
            return null;
    }
    public TraitInGame GetTrait(string traitid)
    {
        if (ContainsTrait(traitid))
            return traits.Where(x => x.id == traitid).ToArray()[0];
        else
            return null;
    }
}

[System.Serializable]
public class Build
{
    public string Name;
    public List<ChampionInGame> champions;
    public Build(string name)
    {
        this.Name = name;
        champions = new List<ChampionInGame>();

        var item = SQLiteUtil.Query(string.Format("SELECT * FROM Build WHERE Build.puuid='{0}' AND Build.name='{1}'", MatchDataManager.Instance.userInfo.puuid, name));
        for (int i = 0; i < item.Count; i++)
        {
            List<Item> items = new List<Item>();
            //for (int j = 0; j < 3; j++)
            //    if (item[i]["item" + j] != null)
            //        items.Add(new Item(int.Parse(item[i]["item" + j].ToString())));

            champions.Add(new ChampionInGame(item[i]["unitID"].ToString(), 1, items.ToArray()));
        }
    }
}
public abstract class DataMonoItem<T> : UnityEngine.MonoBehaviour where T:class
{
    private T _value = null;
    protected System.Action onValueChanged = null;
    public T value
    {
        get => _value;
        set
        {
            if (_value != value)
            {
                _value = value;
                onValueChanged?.Invoke();
            }
            else
                _value = value;
        }
    }
}