﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
namespace DesignPattern
{
    namespace Singleton
    {
        public abstract class Singleton<T> where T : Singleton<T>, new()
        {
            private static T _instance = null;
            public static T Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        _instance = new T();
                    }
                    return _instance;
                }
            }
            public virtual void Initialize() { }
        }
        public abstract class SingletonBehavior<T> : MonoBehaviour where T : SingletonBehavior<T>
        {
            private static T _instance = null;
            public static T Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        if (_instance == null)
                        {
                            GameObject obj = new GameObject();
                            obj.name = typeof(T).Name;
                            obj.AddComponent(typeof(T));
                            _instance = obj.GetComponent<T>();
                            DontDestroyOnLoad(obj);
                        }
                        return _instance;
                    }

                    return _instance;
                }
            }
            public virtual void Initialize() { }
        }
    }
}