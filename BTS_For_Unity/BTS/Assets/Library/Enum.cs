﻿public enum SceneName
{
    Login=0,
    GameRecord=1,
    EditBuild=2,
    Pedigree=3
}
public enum TraitStyleID
{
    NONE,
    BRONZE,
    SILVER,
    GOLD,
    CHROMATIC
}