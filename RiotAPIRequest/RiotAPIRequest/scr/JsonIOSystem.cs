﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace RequestItems
{
    public static class JsonIOSystem
    {
        public static Action onComplete = null;
        public static void Write(string item,string filename)
        {
            int number = 0;
            while (File.Exists(Environment.CurrentDirectory+"/JsonDataOutput/"+ filename + "(" + number + ")" + ".json")) { number += 1; }

            filename = Environment.CurrentDirectory + "/" + filename + (number == 0 ? "" : "(" + number + ")");

            File.WriteAllText(filename + ".json",item.ToString());

            Console.WriteLine(Environment.CurrentDirectory + " 폴더에 파일이 생성되었습니다");
        }
        public static JObject Read(string path)
        {
            if (File.Exists(path))
            {
                return JObject.Parse(File.ReadAllText(path));
            }
            else
            {
                Console.WriteLine("파일이 존재하지 않습니다\n파일경로 : " + path);
                return null;
            }    
        }
    }
}
