﻿using System;
using RequestItems;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace RiotAPIRequest
{
    class Program
    {
        static void Main(string[] args)
        {
            JObject challengers = new JObject();
            JObject grandMasters = new JObject();

            RequestManager.Instance.AddConstraints(new RequestConstraint[]{
                new RequestConstraint(1000, 20),
                new RequestConstraint(120000,100)
            });
            Request request = new RequestLeagueChallenger();
            request.onComplete += (jobject) =>
            {
                challengers = jobject;
            };

            RequestManager.Instance.PushRequest(request);

            request = new RequestLeagueGrandMaster();
            request.onComplete += (jobject) =>
            {
                grandMasters = jobject;
            };

            RequestManager.Instance.PushRequest(request);

            RequestManager.Instance.onCompleted += () =>
            {
                JObject obj = new JObject();
                obj["Challengers"] = challengers["entries"];
                obj["GrandMasters"] = grandMasters["entries"];
                JsonIOSystem.Write(obj.ToString(), "users");
            };

            RequestManager.Instance.StartRequest(0, 10);









            Console.ReadKey();
        }
    }
}
