﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace RequestItems
{
    public class Request
    {
        protected const string REQUEST_FORMAT = "https://kr.api.riotgames.com{0}?api_key=";

        public string http;
        public DateTime timestamp;
        public Action<JObject> onComplete;
    }

    public class RequestSummonerName : Request
    {
        public RequestSummonerName(string name)
        {
            http = string.Format(REQUEST_FORMAT, "/tft/summoner/v1/summoners/by-name/" + name);
        }
    }
    public class RequestSummonerID : Request
    {
        public RequestSummonerID(string ID)
        {
            http = string.Format(REQUEST_FORMAT, "/tft/summoner/v1/summoners/" + ID);
        }
    }


    public class RequestMatch : Request
    {
        public RequestMatch(string puuid, int count)
        {
            http = string.Format(REQUEST_FORMAT, "/tft/match/v1/matches/by-puuid/" + puuid + "?count=" + count);
        }
        public RequestMatch(string matchid)
        {
            http = string.Format(REQUEST_FORMAT, "/tft/match/v1/matches/" + matchid);
        }
    }


    public class RequestLeagueChallenger : Request
    {
        public RequestLeagueChallenger()
        {
            http = string.Format(REQUEST_FORMAT, "/tft/league/v1/challenger");
        }
    }
    public class RequestLeagueGrandMaster : Request
    {
        public RequestLeagueGrandMaster()
        {
            http = string.Format(REQUEST_FORMAT, "/tft/league/v1/grandmaster");
        }
    }
}
