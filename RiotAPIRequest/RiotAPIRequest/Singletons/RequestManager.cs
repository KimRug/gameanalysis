﻿using System;
using System.Collections.Generic;
using RequestItems;
using System.Threading;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net;

namespace RiotAPIRequest
{
    class RequestManager
    {
        private const string apiKey = "RGAPI-ce60c824-3674-43ed-a55f-a56b66999e18";
        private static RequestManager _instance = null;
        public static RequestManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new RequestManager();

                return _instance;
            }
        }

        private Timer sendTimer = null;
        public bool isRunning = false;
        RequestConstraint[] constraints;
        Queue<Request> requestQueue;
        DateTime timeStamp;
        public event Action onCompleted = null;
        public RequestManager()
        {
            constraints = null;
            requestQueue = new Queue<Request>();
        }

        public void AddConstraints(RequestConstraint[] constraints)
        {
            this.constraints = constraints;
        }
        public void StartRequest(int delayMilliSecond, int intervalMilliSecond)
        {
            if (constraints != null)
                constraints = constraints.OrderBy(item => item.milliSecond).ToArray();

            isRunning = true;
            timeStamp = DateTime.Now;
            Console.WriteLine("#######     Request sending was start.      #######");
            SendRequest();
            //sendTimer = new Timer(SendRequest, null, delayMilliSecond, intervalMilliSecond);
        }
        public void StopRequest()
        {
            isRunning = false;
            sendTimer = null;
            onCompleted?.Invoke();
            Console.WriteLine("#######     Request sending was stoped.     #######");
        }

        public void PushRequest(Request request)
        {
            requestQueue.Enqueue(request);
        }

        private void SendRequest()
        {
            JObject output;
            int minSleepSecond = 0;
            if (constraints != null)
                minSleepSecond = constraints.Select(item => item.milliSecond).Min();

            while (requestQueue.Count != 0)
            {
                Request request = requestQueue.Dequeue();

                while (true)
                {
                    try
                    {
                        if (constraints != null)
                        {
                            for (int i = 0; i < constraints.Length; i++)
                            {
                                if (!constraints[i].canRequest)
                                    Thread.Sleep(constraints[i].lastTime);
                            }
                        }
                        Console.WriteLine("요청한 http\n" + request.http + apiKey + "\n");
                        output = JObject.Parse(new WebClient().DownloadString(request.http + apiKey));
                        for (int i = 0; i < constraints.Length; i++)
                        {
                            request.timestamp = DateTime.Now;
                            constraints[i].requestedQueue.Enqueue(request);
                        }
                        request.onComplete.Invoke(output);
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("@@@@@@에러 발생@@@@@@\n" + e + "\n@@@@@@ " + minSleepSecond / 1000 + "초후 재요청 예정@@@@@@");
                        Thread.Sleep(minSleepSecond);
                        continue;
                    }
                }
            }

            StopRequest();
        }
        ~RequestManager()
        {
            StopRequest();
        }
    }
    public class RequestConstraint
    {
        public int milliSecond;
        public int requestCount;
        public bool canRequest => requestedQueue.Count < requestCount;
        public int lastTime
        {
            get
            {
                if (lastRequest == null)
                    return 0;

                return milliSecond - (int)(DateTime.Now - lastRequest.timestamp).TotalMilliseconds;
            }
        }

        public Queue<Request> requestedQueue;
        public Request lastRequest;

        private Timer remover;

        public RequestConstraint(int milliSecond, int requestCount)
        {
            this.milliSecond = milliSecond;
            this.requestCount = requestCount;
            lastRequest = null;
            requestedQueue = new Queue<Request>();
            remover = new Timer(ExcutedRequestRemover, null, 0, milliSecond + 1);
        }
        ~RequestConstraint()
        {
            remover.Dispose();
            remover = null;
        }

        public string To_str()
        {
            return milliSecond / 1000 + "초에 " + requestCount + "개의 요청";
        }

        public void ExcutedRequestRemover(object state)
        {
            if (requestedQueue.Count == 0)
                return;

            else if (lastRequest == null)
            {
                lastRequest = requestedQueue.Dequeue();
                return;
            }

            if (lastRequest.timestamp.AddMilliseconds(milliSecond) > DateTime.Now)
            {
                lastRequest = requestedQueue.Dequeue();
            }
        }
    }
}
